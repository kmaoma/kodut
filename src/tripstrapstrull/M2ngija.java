package tripstrapstrull;

/*
 * esitab m�ngu m�ngijat
 * */
public class M2ngija {
	private int v6ite = 0;
	private String m2ngijaSymbol;
	
	//loo uus m�ngija etteantud s�mboliga
	M2ngija(String m2ngijaSymbol) {
		this.m2ngijaSymbol = m2ngijaSymbol;
	}
	
	//tagasta m�ngija v�itude arv
	public int tagastaV6itudeArv() {
		return v6ite;
	}

	//suurendab m�ngija v�itude arvu 1 v�rra
	public void suurendaV6itudeArvu() {
		v6ite++;
	}

	//tagastab m�ngija k�ike esindava s�mboli
	public String tagastaM2ngijaSymbol() {
		return m2ngijaSymbol;
	}

	//m��ra m�ngija k�ike esindav s�mbol
	public void m22raM2ngijaSymbol(String m2ngijaSymbol) {
		this.m2ngijaSymbol = m2ngijaSymbol;
	}
	
	//kui m�ngijat kontrollib inimene, tagasta t�ene v��rtus
	public boolean onInimene() {
		return m2ngijaSymbol.equals("X");
	}
}
