package tripstrapstrull;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * Kuulab kasutaja hiireklikke
 * */
public class Kuulaja implements ActionListener{
    M2ng m2ng;

    Kuulaja(M2ng m2ng) {
        super();
        this.m2ng = m2ng;
    }

    //kui klikitav ruut on t�hi, siis tee sinna k�ik
    public void actionPerformed(ActionEvent e) {
    	Ruut ruut = (Ruut) e.getSource();
        if (ruut.onTyhi()) {
        	ruut.joonistaRuuduleSymbol(m2ng.tagastaM2ngijaInimene().tagastaM2ngijaSymbol());
        	m2ng.j2rgmineK2ik();
        }
    }
}