package tripstrapstrull;

/*
 * M�nguloogika klassis on meetodid v�idu ja viigi kontrollimiseks
 * */
public class M2nguLoogika {
	public static boolean kasOnV6it(Laud m2ngulaud) {
		if (horisontaalneV6it(m2ngulaud.tagastaRuudud())
				|| vertikaalneV6it(m2ngulaud.tagastaRuudud())
				|| diagonaalneV6it(m2ngulaud.tagastaRuudud())
			)
			return true;
		else
			return false;
	}
	
	//kui k�ik ruudud on t�is, l�ppeb m�ng viigiga
	public static boolean kasOnViik(Laud m2ngulaud) {
		int ruuteT2is = 0;
		for (Ruut[] rida : m2ngulaud.tagastaRuudud()) {
			for (Ruut ruut : rida) {
				if (!ruut.onTyhi())
					ruuteT2is++;
			}
		}
		
		return ruuteT2is == 9;
	}
	
	//Kui 3 sama s�mboliga ruutu on j�rjest horisontaalselt, on m�ng v�idetud
	private static boolean horisontaalneV6it(Ruut[][] ruudud) {
		int j2rjest = 0;

        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 2; j++) {
                if(ruudud[i][j].getText().equals(ruudud[i][j+1].getText()) && !ruudud[i][j].onTyhi())
                	j2rjest += 1;
            }

            if(j2rjest == 2) return true;
            else j2rjest = 0;
        }
        
        return false;
	}
	
	//Kui 3 sama s�mboliga ruutu on j�rjest vertikaalselt, on m�ng v�idetud
	private static boolean vertikaalneV6it(Ruut[][] ruudud) {
		int j2rjest = 0;
		
		for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 2; j++) {
                if(ruudud[j][i].getText().equals(ruudud[j+1][i].getText()) && !ruudud[j][i].onTyhi())
                	j2rjest += 1;
            }

            if(j2rjest == 2) return true;
            else j2rjest = 0;
        }
		
		return false;
	}
	
	//Kui 3 sama s�mboliga ruutu on j�rjest diagonaalselt, on m�ng v�idetud
	private static boolean diagonaalneV6it(Ruut[][] ruudud) {
		int j2rjest = 0;
		
		for (int i = 0; i < 2; i++) {
            if(ruudud[i][i].getText().equals(ruudud[i+1][i+1].getText()))
            	j2rjest += 1;
        }
        if (j2rjest == 2 && !ruudud[0][0].onTyhi())
            return true;
        j2rjest = 0;

        for (int i = 0; i < 2; i++) {
            if(ruudud[i][2-i].getText().equals(ruudud[i+1][2-(i+1)].getText()))
            	j2rjest += 1;
        }
        if (j2rjest == 2 && !ruudud[0][2].onTyhi())
            return true;

        return false;
	}
}
