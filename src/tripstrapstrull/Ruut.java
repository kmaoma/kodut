package tripstrapstrull;

import javax.swing.JButton;

/*
 * esitab m�ngulaual olevat ruutu
 * */
public class Ruut extends JButton{
	private static final long serialVersionUID = 1L;
	
	//loo uus ruut
	Ruut() {
		super();
	}

	//kontrolli, kas ruut on t�hi
	public boolean onTyhi() {
    	return this.getText().equals("");
    }
    
	//joonista ruudule etteantud s�mbol
    public void joonistaRuuduleSymbol(String symbol) {
    	this.setText(symbol);
    }
    
    //muuda ruut t�hjaks
    public void algseadista() {
    	this.setText("");
    }
}
