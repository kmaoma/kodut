package tripstrapstrull;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Font;

/*
 * esitab m�ngulauda
*/
public class Laud extends JFrame{
	private static final long serialVersionUID = 1L;
	private Ruut[][] ruudud = new Ruut[3][3];
    private M2ng m2ng;
    public JLabel kasutajaV6ite;
    public JLabel arvutiV6ite;

    //loo uus m�ngulaud
    Laud(M2ng m2ng) {
        this.m2ng = m2ng;
        JPanel m2nguLaud = new JPanel(new GridLayout(1,2));
        joonistaRuudustik(m2nguLaud);
        joonistaTulemusteTabel(m2nguLaud);
        
        this.getContentPane().add(m2nguLaud);
        
        setSize(800, 400);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    //joonista 9x9 ruudustik ja lisa see m�ngulauale
    private void joonistaRuudustik(JPanel m2nguLaud) {
    	JPanel ruudustik = new JPanel(new GridLayout(3,3));

        for (int rida = 0; rida < 3; rida++) {
            for (int veerg = 0; veerg < 3; veerg++) {
            	ruudud[rida][veerg] = looUusRuut();
            	ruudustik.add(ruudud[rida][veerg]);
            }
        }
        m2nguLaud.add(ruudustik, BorderLayout.LINE_START);
    }
    
    //joonista m�ngijate tulemusi esitav tabel ja lisa see m�ngulauale
    private void joonistaTulemusteTabel(JPanel m2nguLaud) {
    	JPanel tulemustePaneel = new JPanel(new GridLayout(2,1));
    	JLabel tulemustePealkiri = new JLabel("Tulemused", SwingConstants.CENTER);
    	tulemustePealkiri.setFont(new Font("Arial", Font.BOLD, 40));
    	tulemustePaneel.add(tulemustePealkiri);
    	
    	JPanel tulemused = new JPanel(new GridLayout(3,1));
    	kasutajaV6ite = new JLabel("Mina: 0", SwingConstants.CENTER);
    	arvutiV6ite = new JLabel("Arvuti: 0", SwingConstants.CENTER);
    	
    	kasutajaV6ite.setFont(new Font("Arial", Font.BOLD, 20));
    	arvutiV6ite.setFont(new Font("Arial", Font.BOLD, 20));
    	
    	tulemused.add(kasutajaV6ite);
    	tulemused.add(arvutiV6ite);
    	tulemustePaneel.add(tulemused);
    	
    	m2nguLaud.add(tulemustePaneel, BorderLayout.LINE_END);
    }
    
    //loo uus ruut ja lisa talle kuulaja, mis ootab kasutaja hiireklikke
    private Ruut looUusRuut() {
    	Ruut ruut = new Ruut();
    	ruut.setFont(new Font(ruut.getFont().getName(), Font.BOLD, 40));
    	ruut.addActionListener(new Kuulaja(this.m2ng));
    	return ruut;
    }

	//tagasta m�ngulaua ruutude jada
    public Ruut[][] tagastaRuudud() {
        return ruudud;
    }
    
    //tagasta etteantud real ja veerul asetsev ruut
    public Ruut tagastaRuutPositsioonil(int rida, int veerg) {
    	return ruudud[rida][veerg];
    }
    
    //algseadista k�ik m�ngulaua ruudud
    public void algseadista() {
    	for (int rida = 0; rida < 3; rida++) {
            for (int veerg = 0; veerg < 3; veerg++) {
            	ruudud[rida][veerg].algseadista();
            }
        }
    }
    
    //uuenda m�ngijate tulemuste silte
    public void uuendaTulemusteSilte() {
    	kasutajaV6ite.setText("Mina: " + m2ng.tagastaM2ngijaInimene().tagastaV6itudeArv());
    	arvutiV6ite.setText("Arvuti: " + m2ng.tagastaM2ngijaArvuti().tagastaV6itudeArv());
    }
}