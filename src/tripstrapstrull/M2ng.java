package tripstrapstrull;

import javax.swing.JOptionPane;

/*
 * Esitab m�ngu seisu
*/
public class M2ng {
    private Laud laud;
    private M2ngija inimene;
    private M2ngija arvuti;
    private M2ngija aktiiveM�ngija;

    //Loo uus m�ngulaud ja m�ngijad
    M2ng() {
    	laud = new Laud(this);
    	inimene = new M2ngija("X");
    	arvuti = new M2ngija("O");
    }

    //taasta m�ngulaua algseis ja alusta uut m�ngu
    public void alustaUutM2ngu() {
    	laud.algseadista();
        String[] options = {"Mina (X)", "Arvuti (O)"};
        int alustavM2ngija = JOptionPane.showOptionDialog(null, "Kes alustab?", 
                "Trips-Traps-Trull", JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        
        if(alustavM2ngija == 0)
        	aktiiveM�ngija = inimene;
        else {
        	aktiiveM�ngija = arvuti;
        	suvalineK2ik();
        }
    }
    
    //tagasta m�ngulaud
    public Laud tagastaM2ngulaud() {
    	return laud;
    }

    //tagasta m�ngija, kelle k�ik parasjagu on
    public M2ngija tagastaAktiivneM2ngija() {
    	return aktiiveM�ngija;
    }

    //tagasta inimese poolt kontrollitav m�ngija
    public M2ngija tagastaM2ngijaInimene() {
    	return inimene;
    }
    
    //tagasta arvuti poolt kontrollitav m�ngija
    public M2ngija tagastaM2ngijaArvuti() {
    	return arvuti;
    }
    
    //vaheta hetkel aktiivset m�ngijat
    public void vahetaAktiivsetM2ngijat() {
    	if (inimene == aktiiveM�ngija)
    		aktiiveM�ngija = arvuti;
    	else
    		aktiiveM�ngija = inimene;
    }

    //leia t�hi ruut ja tee sinna k�ik
    public void suvalineK2ik() {
        while(true) {
            int rida = Math.round((float) Math.random() * 2);
            int veerg = Math.round((float) Math.random() * 2);
            Ruut ruut = laud.tagastaRuutPositsioonil(rida, veerg);
            if (ruut.onTyhi()) {
            	ruut.joonistaRuuduleSymbol(aktiiveM�ngija.tagastaM2ngijaSymbol());
            	break;
            }
        }
        j2rgmineK2ik();
    }

    //kuva kasutajale teade, kes m�ngu v�itis
    public void kuvaV6iduTekst() {
        if (aktiiveM�ngija.onInimene()) {
            JOptionPane.showMessageDialog(null, "Sina v�itsid!", "l�pp", JOptionPane.OK_OPTION);
        }
        else {
            JOptionPane.showMessageDialog(null, "V�itja on arvuti!", "l�pp", JOptionPane.OK_OPTION);
        }
    }
    
    //kuva kasutajale teade, et m�ng j�i viiki
    public void kuvaViigiTekst() {
    	JOptionPane.showMessageDialog(null, "M�ng j�i viiki!", "l�pp", JOptionPane.OK_OPTION);
    }
    
    //l�peta m�ng ja k�si kasutajalt, kas ta tahab uut m�ngu alustada
    public void l6petaM2ng() {
    	String[] valikud = {"Jah", "Ei"};
        int uusM2ng = JOptionPane.showOptionDialog(null, "Uus m�ng?", 
                "Trips-Traps-Trull", JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, valikud, valikud[0]);
        if (uusM2ng == 0) {
        	alustaUutM2ngu();
        }
        else {
        	laud.dispose();
        }
    }
    
    /*kui keegi v�itis v�i m�ng j�i viiki, siis l�peta m�ng ja uuenda m�ngijate tulemusi
    vastasel korral on j�rgmise m�ngija kord*/
    public void j2rgmineK2ik() {
    	if (M2nguLoogika.kasOnV6it(laud)) {
    		kuvaV6iduTekst();
    		aktiiveM�ngija.suurendaV6itudeArvu();
    		laud.uuendaTulemusteSilte();
    		l6petaM2ng();
    	}
    	else if (M2nguLoogika.kasOnViik(laud)) {
    		kuvaViigiTekst();
    		l6petaM2ng();
    	}
    	else {
    		vahetaAktiivsetM2ngijat();
        	if (!aktiiveM�ngija.onInimene())
        		suvalineK2ik();
    	}
    }
}